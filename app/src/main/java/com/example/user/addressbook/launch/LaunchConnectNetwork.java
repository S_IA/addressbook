package com.example.user.addressbook.launch;

import android.os.AsyncTask;
import android.util.Log;

import com.example.user.addressbook.HttpsNetwork;

import okhttp3.Response;


public class LaunchConnectNetwork<T> extends AsyncTask<Void, Void, Void> {


    public interface TaskCompleted<T> {
        void onTaskCompleted(T response);
    }

    private TaskCompleted listener;
    private Response response;
    private Class<T> value;

    public LaunchConnectNetwork(TaskCompleted listener, Class<T> clazzT) {
        this.listener = listener;
        this.value = clazzT;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.d("myLog", "AsynTask - End");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("myLog", "AsynTask - Begin");
    }

    @Override
    protected Void doInBackground(Void... voids) {

        T data = null;
        try {
            if (isCancelled()) return null;
            else {

                data = (T) new HttpsNetwork<T>().getInstance().helloy(value);
            }
            if (isCancelled()) return null;
            else {
                listener.onTaskCompleted(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        new HttpsNetwork<T>().getInstance().cancelAll();
        Log.d("myLog", "Cancel");
        return;

    }
}


