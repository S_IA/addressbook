package com.example.user.addressbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.user.addressbook.listDepartments.Department;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class ControlDbHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "MySql";
    private static final String DATABASE_NAME = "D_DB.db";
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE = "table1";
    public static final String DATABASE_TABLE_PHOTO = "table_Photo";

    // поля таблицы для хранения (id формируется автоматически)
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LIST = "JsonList";
    public static final String COLUMN_PROFILE_ID = "ID_Profile";
    public static final String COLUMN_PHOTO = "Photo";


    // формируем запрос для создания базы данных списска
    private static final String DATABASE_CREATE = "create table "
            + DATABASE_TABLE + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_LIST + " string" + ");";

    // формируем запрос для создания базы данных фотографий
    private static final String DATABASE_CREATE_TABLE_PHOTO = "create table "
            + DATABASE_TABLE_PHOTO + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_PROFILE_ID + " string,"
            + COLUMN_PHOTO + " blob" + ");";

    /**
     * Инициализация
     *
     * @param context
     */
    public ControlDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Создание БД
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");
        //создаем таблицу
        db.execSQL(DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE_TABLE_PHOTO);

    }

    /**
     * Обновление БД
     *
     * @param db         - БД
     * @param oldVersion - текущая версия БД
     * @param newVersion - новая версия БД
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS table1");
        onCreate(db);
    }

    /**
     * Создаёт новый контакт.
     */
    public long createNewRecord(String record) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_ID, record);

        long row = db.insert(DATABASE_TABLE, null, values);
        db.close();

        return row;
    }

    /**
     * Добавление фотографии в БД
     *
     * @param id
     * @param image
     * @throws SQLiteException
     */
    public void addEntry(String id, byte[] image) throws SQLiteException {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_PROFILE_ID, id);
        cv.put(COLUMN_PHOTO, image);
        db.insert(DATABASE_TABLE_PHOTO, null, cv);

    }

    /**
     * Получаем конкретный контакт
     */
    public Cursor getRow(long rowId) throws SQLException {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.query(true, DATABASE_TABLE,
                new String[]{COLUMN_ID, COLUMN_LIST}, COLUMN_ID + "=" + rowId, null,
                null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Удаление контакта
     */
    public void deleteRow(long rowId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_TABLE, COLUMN_ID + "=" + rowId, null);
        db.close();
    }

    /**
     * Изменение строчки
     */
    public void updateRecord(long rowId, String record) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues updateValues = createContentValues(record);
        db.update(DATABASE_TABLE, updateValues, COLUMN_ID + "=" + rowId,
                null);
    }

    /**
     * Описываем структуру данных
     */
    private ContentValues createContentValues(String record) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_LIST, record);
        return values;
    }

    /**
     * Проверка выборки существования записи в таблици
     *
     * @param db
     * @param idProfile
     * @return
     */
    public boolean selectPhoto(ControlDbHelper db, String idProfile) {
        SQLiteDatabase con = db.getReadableDatabase();
        Cursor cursor = con.query(DATABASE_TABLE_PHOTO,
                new String[]{COLUMN_ID, COLUMN_PROFILE_ID, COLUMN_PHOTO},
                COLUMN_PROFILE_ID + "=?", new String[]{idProfile},
                null, null, null);
        boolean result = false;
        if (cursor.getCount() > 0) {
            result = true;
        }
        cursor.close();
        return result;
    }


    /**
     * Получения данных из таблици tabel*
     *
     * @param db
     * @return
     */
    public Department extractDepartment(ControlDbHelper db) {
        Cursor cursor = db.getRow(1);
        int column = cursor.getColumnIndex("JsonList");
        Department department;
        department = new Gson().fromJson(cursor.getString(column), Department.class);
        cursor.close();
        return department;
    }

    /**
     * Получение фотографии из БД
     *
     * @return
     */
    public Bitmap onDataLoadFromDB(ControlDbHelper db, String idProfile) {


        SQLiteDatabase con = db.getReadableDatabase();
        Cursor cursor = con.query(DATABASE_TABLE_PHOTO,
                new String[]{COLUMN_ID, COLUMN_PROFILE_ID, COLUMN_PHOTO},
                COLUMN_PROFILE_ID + "=?", new String[]{idProfile},
                null, null, null);
        Bitmap bmp = null;
        if((cursor!= null) && (cursor.moveToFirst())) {
            byte[] blob = (byte[]) cursor.getBlob(cursor.getColumnIndex(COLUMN_PHOTO));
            InputStream inputStream = new ByteArrayInputStream(blob);
           bmp = BitmapFactory.decodeByteArray(blob, 0, blob.length);
           cursor.close();
        }
        return bmp;
    }
}
