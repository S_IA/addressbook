package com.example.user.addressbook;

import java.util.HashMap;
import java.util.Map;

public class UserAttributes {

    private static UserAttributes instance;
    private Map<String, String> arguments;


    public  UserAttributes getInstance() {
        if(instance == null){
            instance = new UserAttributes();
        }
        return instance;
    }

    /**
     * задать атрибуты пользователя
     * @param login - логин пользовател
     * @param password - пароль пользователя
     */
    public void setUser(String login, String password){
        arguments = new HashMap<String, String>();
        arguments.put("login", login);
        arguments.put("password", password);

    }

    /**
     * задать ID  фотографии
     * @param id
     */
    public void setID(String id){
        arguments.put("id", id);

    }


    /**
     * Передача карты параметров
     * @return
     */
    public Map<String, String> getMapArguments() {
        return arguments;
    }
}
