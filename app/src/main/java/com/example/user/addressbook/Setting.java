package com.example.user.addressbook;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.user.addressbook.InterfaceProject.InterfModel;

public class Setting implements InterfModel {
    private SharedPreferences setting;

    private static final String APP_PREFERENCES = "mysettings";
    private static final String APP_LOGIN_USER = "LoginUser";
    private static final String APP_PASSWORD_USER = "PasswordUser";
    private static final String APP_CHECK_SAVE_USER = "ChekSaveUser";
    private static final String APP_HAS_VISITED = "hasVisited";

    /**
     * Получение эземляра для получения данных настроек в коде приложения
     *
     * @param context
     */
    public Setting(Context context) {
        setting = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Получения логина
     * @return
     */
    public String getSettigLogin() {
        return setting.getString(APP_LOGIN_USER, "");
    }

    /**
     * Получение данных о первичном входе в приложение
     * @return
     */
    public Boolean getSettingVisited() {
        return setting.getBoolean(APP_HAS_VISITED, false);
    }

    /**
     * Пользователь зарегистрировался
     * @param login
     * @param password
     */
    public void hasVisitedFalse(String login, String password) {
        SharedPreferences.Editor e = setting.edit();
        e.putBoolean("hasVisited", true);
        e.putString(APP_LOGIN_USER, login);
        e.putString(APP_PASSWORD_USER, password);
        e.commit();
    }

    /**
     * Удалить пользователя
     */
    public void deletedUser() {
        SharedPreferences.Editor e = setting.edit();
        e.remove(APP_LOGIN_USER);
        e.remove(APP_PASSWORD_USER);
        e.remove("hasVisited");
        e.putBoolean("hasVisited", false);
        e.commit();
    }


    /**
     * Получение пароля
     * @return
     */
    public String getSettingPass() {
        return setting.getString(APP_PASSWORD_USER, "");
    }

    /**
     * Получение подтверждения атрибута
     * "Сохранение данных пользователя
     * и выво их при следующе запуске приложения"
     * @return
     */
    public boolean getSaveUser() {
        return (boolean) setting.getBoolean(APP_CHECK_SAVE_USER, false);
    }

    /**
     * Добавить/ удалить атрибуты пользователя
     * @param login - новый логин пользователя
     * @param password - новый пароль пользователя
     */
    public void setSaveUser(String login, String password) {
        if (setting.getBoolean(APP_CHECK_SAVE_USER, false)) {
            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor e = setting.edit();
            e.remove(APP_CHECK_SAVE_USER);
            e.remove(APP_LOGIN_USER);
            e.remove(APP_PASSWORD_USER);
            e.putBoolean(APP_CHECK_SAVE_USER, false);
            e.commit();
        } else {
            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor e = setting.edit();
            e.remove(APP_CHECK_SAVE_USER);
            e.putBoolean(APP_CHECK_SAVE_USER, true);
            e.putBoolean("hasVisited", true);
            e.putString(APP_LOGIN_USER, login);
            e.putString(APP_PASSWORD_USER, password);
            e.commit();
        }
    }


}
