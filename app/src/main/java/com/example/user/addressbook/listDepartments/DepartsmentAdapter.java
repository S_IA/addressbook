package com.example.user.addressbook.listDepartments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.user.addressbook.R;

import java.util.ArrayList;
import java.util.LinkedList;


public class DepartsmentAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater; // класс позволяющий вставлять в View разметку из xml файла
    private ArrayList<Pair> hierarchyArray; //  Это массив, который будет содержать видимые элементы иерархии и который мы собственно и будем отрисовывать.
    private ArrayList<Department> originalDepatment; // Массив элементов верхнего уровня иерархии. Этот список передается конструктору адаптера, он нам нужен, чтобы обновлять массив hierarchyArray при открытии/закрытии какого-то элемента.
    private LinkedList<Department> openDepartments; //  Массив открытых элементов


    private class Pair {
        Department department;
        Employees employees;

        int level;

        Pair(Department department, int level) {
            this.department = department;
            this.level = level; //Уровень иерархии
        }

        Pair(Employees employees, int level) {
            this.employees = employees;
            this.level = level;
        }

    }

    public void clear() {
        originalDepatment = null;
        hierarchyArray = null;
        openDepartments = null;
    }


    /**
     * Инициализация
     *
     * @param context - контехст активити
     * @param dep     - объект
     */
    public DepartsmentAdapter(Context context, Department dep) {

        mLayoutInflater = LayoutInflater.from(context);
        originalDepatment = new ArrayList<Department>();
        originalDepatment.add(dep);

        hierarchyArray = new ArrayList<Pair>();
        openDepartments = new LinkedList<Department>();

        generationHierarchy();

    }

    /**
     * Генерация иерархии
     */
    private void generationHierarchy() {
        hierarchyArray.clear();
        generationList(originalDepatment, 0);
    }

    /**
     * Генерация списка
     *
     * @param departments - список объектов
     * @param level       - уровень иерархии
     */
    private void generationList(ArrayList<Department> departments, int level) {
        for (Department i : departments) {
            hierarchyArray.add(new Pair(i, level));
            if (openDepartments.contains(i)) {

                if (i.getEmployees() != null) {
                    if (i.getEmployees().size() > 0)
                        generationListEmployees(i.getEmployees(), level + 1);
                }
                if (i.getDepartments() != null) {
                    if (i.getDepartments().size() > 0)
                        generationList(i.getDepartments(), level + 1);
                }
            }
        }
    }

    /**
     * Генерация сотрудников
     *
     * @param employees - список сотрудников
     * @param level     - уровень иерархии
     */
    private void generationListEmployees(ArrayList<Employees> employees, int level) {
        for (Employees i : employees) {
            hierarchyArray.add(new Pair(i, level));
        }
    }

    /**
     * Обработка нажатия на элемент
     *
     * @param position - позиция элемента
     * @return
     */
    public boolean clickOnItem(int position) {
        Pair i = hierarchyArray.get(position);
        if (i.department != null) {
            if (i.department.getDepartments() != null) {
                if (i.department.getDepartments().size() > 0) {
                    if (!closeDepartments(i.department))
                        openDepartments.add(i.department);

                    generationHierarchy();
                    notifyDataSetChanged();
                    return false;

                }

            }
            if (i.department.getEmployees() != null) {
                if (i.department.getEmployees().size() > 0) {
                    if (!closeDepartments(i.department))
                        openDepartments.add(i.department);

                    generationHierarchy();
                    notifyDataSetChanged();
                    return false;

                }
            }

        }
        if (i.employees != null) {
            return true;
        }
        return false;
    }

    /**
     * Указание откытый/закрытый список
     *
     * @param i - элемент
     * @return
     */
    private boolean closeDepartments(Department i) {
        if (openDepartments.remove(i)) {
            if (i.getDepartments() != null) {
                for (Department c : i.getDepartments())
                    closeDepartments(c);
            }
            return true;
        }
        return false;
    }

    /**
     * Получение элемента
     *
     * @param position - позиция элемента
     * @return
     */
    public Employees getItemEmployees(int position) {
        return hierarchyArray.get(position).employees;
    }

    /**
     * Получение размера списка иерархии
     *
     * @return
     */
    @Override
    public int getCount() {
        return hierarchyArray.size();
    }

    /**
     * Получения элемента из списка иерархии
     *
     * @param position - позиция элемента
     * @return
     */
    @Override
    public Object getItem(int position) {
        return hierarchyArray.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Отрисовка пункта списка
     *
     * @param position    - позиция элемента
     * @param convertView - элемент отображения
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = mLayoutInflater.inflate(R.layout.row, null);
        TextView title = convertView.findViewById(R.id.aa_Name);

        Pair pair = hierarchyArray.get(position);

        if (pair.department != null) {
            title.setText(pair.department.getName() + "\n" + pair.department.getId());

            title.setCompoundDrawablesWithIntrinsicBounds(pair.department.getIconResource(), 0, 0, 0);

            title.setPadding(pair.level * 30, 0, 0, 0);

        }
        if (pair.employees != null) {
            title.setText(pair.employees.getName() + "\n" + pair.employees.getId());

            title.setCompoundDrawablesWithIntrinsicBounds(pair.employees.getIconResource(), 0, 0, 0);

            title.setPadding(pair.level * 30, 0, 0, 0);
        }
        return convertView;
    }

}
