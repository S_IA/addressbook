package com.example.user.addressbook;

import com.google.gson.annotations.SerializedName;


public class Authentication {

    @SerializedName("Message")
    private String Message;
    @SerializedName("Success")
    private boolean Success;

    Authentication() {
        Message = null;
        Success = false;
    }

    /**
     * Сообщение
     * @return
     */
    public String getMessage() {
        return Message;
    }

    /**
     * Подтверждение аутентификации
     * @return
     */
    public boolean getSuccess() {
        return Success;
    }


}
