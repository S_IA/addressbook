package com.example.user.addressbook.listDepartments;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.user.addressbook.R;
import com.google.gson.annotations.SerializedName;

public class Employees implements Parcelable {

    @SerializedName("ID")
    private String id;
    @SerializedName("Name")
    private String name;
    @SerializedName("Title")
    private String title;
    @SerializedName("Email")
    private String email;
    @SerializedName("Phone")
    private String phone;


    protected Employees(Parcel in) {
        id = in.readString();
        name = in.readString();
        title = in.readString();
        email = in.readString();
        phone = in.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(title);
        dest.writeString(email);
        dest.writeString(phone);
    }

    public static final Creator<Employees> CREATOR = new Creator<Employees>() {
        @Override
        public Employees createFromParcel(Parcel in) {
            return new Employees(in);
        }

        @Override
        public Employees[] newArray(int size) {
            return new Employees[size];
        }
    };

    /**
     * Получение id
     */
    public String getId() {
        return id;
    }

    /**
     * Получение имени
     */
    public String getName() {
        return name;
    }

    /**
     * Получение title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Получение email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Получение phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Задаем id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Задаем имя
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Задаем title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Задаем email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Задаем phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Получение визуального обозначения
     */
    public int getIconResource() { // Метод возвращающий картинку. Если есть потомки, то будет иконка в виде папки, если нет, то в виде файла.

        return R.drawable.baseline_account_box_black_18dp;

    }


}
