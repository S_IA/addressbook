package com.example.user.addressbook.InterfaceProject;

import android.content.Context;
import android.support.annotation.StringRes;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public interface InterfView {
    void showText(String string);

    void showMessage(@StringRes int string);

    void showProgress();

    void hideProgress();

    Context getContext();

    void onButtonWasClickedEnter() throws InterruptedException, ExecutionException, TimeoutException, IOException;


    void newActivity();

    void onDestroy();

    void showAlertDialog();
}
