package com.example.user.addressbook.profile;

import android.os.AsyncTask;

import com.example.user.addressbook.HttpsNetwork;

import java.net.MalformedURLException;

public class ProfileConnectNetwork<T> extends AsyncTask<Void, Void, Void> {


    public interface TaskCompleted<T> {
        void onTaskCompleted(T response);
    }

    private TaskCompleted listener;
    private Class<T> value;


    public ProfileConnectNetwork(TaskCompleted listener, Class<T> clazzT) {
        this.listener = listener;
        this.value = clazzT;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

    @Override
    protected Void doInBackground(Void... voids) {


        T data = null;
        try {
            data = (T) new HttpsNetwork<T>().getInstance().getWPhoto();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        listener.onTaskCompleted(data);
        return null;
    }
}
