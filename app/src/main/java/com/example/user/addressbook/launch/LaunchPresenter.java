package com.example.user.addressbook.launch;

import android.util.Log;

import com.example.user.addressbook.Authentication;
import com.example.user.addressbook.InterfaceProject.InterfPresenter;
import com.example.user.addressbook.Setting;
import com.example.user.addressbook.UserAttributes;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LaunchPresenter implements InterfPresenter {


    private LaunchActivity mView;
    private Setting setting;


    public LaunchPresenter(LaunchActivity mView) {
        this.mView = mView;
        setting = new Setting(mView.getContext());
    }

    @Override
    public Setting getSetting() {
        return setting;
    }

    /**
     * Функция начала аутентификации
     *
     */
    void beginAuthem() {

        LaunchConnectNetwork.TaskCompleted<Authentication> listener = new LaunchConnectNetwork.TaskCompleted<Authentication>() {
            @Override
            public void onTaskCompleted(final Authentication response) {
                if (response != null) {
                    mView.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.getSuccess()) {
                                mView.showText("Успешное подтверждение пользователя.");
                                mView.newActivity();
                            } else {
                                mView.showText(response.getMessage());
                            }
                            mView.hideProgress();
                        }
                    });

                }
            }
        };

        LaunchConnectNetwork<Authentication> con = new LaunchConnectNetwork<Authentication>(listener, Authentication.class);
        try {
            con.execute().get(3000, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            e.printStackTrace();
            con.cancel(true);
            getMessage("Превышенно время соединения");
            Log.e("MyError", "TimeoutException -- LaunchPresenter");
            mView.hideProgress();
            mView.showAlertDialog();
        } catch (InterruptedException e) {
            e.printStackTrace();
            mView.hideProgress();
            con.cancel(true);
            Log.e("MyError", "InterruptException -- LaunchPresenter");
        } catch (ExecutionException e) {
            e.printStackTrace();
            mView.hideProgress();
            con.cancel(true);
            Log.e("MyError", "ExecuitionException -- LaunchPresenter");
        }

    }


    /**
     * Запуск анимации загрузки
     */
    @Override
    public void runProgtressBar() {
        mView.showProgress();
    }

    /**
     * Завершение анимации загрузки
     */
    @Override
    public void hideProgressBar() {
        mView.hideProgress();
    }

    /**
     * Вывод сообщения
     * @param string - текст сообщения
     */
    @Override
    public void getMessage(String string) {
        mView.showText(string);
    }

    @Override
    public void onDestroy() {
        mView = null;
        setting = null;
    }


    public void setUserAttridutes(String login, String password) {
        new UserAttributes().getInstance().setUser(login, password);
    }
}
