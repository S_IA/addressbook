package com.example.user.addressbook.profile;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.example.user.addressbook.ControlDbHelper;
import com.example.user.addressbook.InterfaceProject.InterfPresenter;
import com.example.user.addressbook.Setting;
import com.example.user.addressbook.UserAttributes;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ProfilePresenter implements InterfPresenter {



    private ProfileActivity pView;
    private ImageView imageView;

    public ProfilePresenter(ProfileActivity view, ImageView image) {
        pView = view;
        imageView = image;

    }

    @Override
    public void runProgtressBar() {
        pView.showProgress();
    }

    @Override
    public void hideProgressBar() {
        pView.hideProgress();
    }

    @Override
    public void getMessage(String string) {
        pView.showText(string);
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public Setting getSetting() {
        return null;
    }


    public void getImage( final ControlDbHelper db) {

        ProfileConnectNetwork.TaskCompleted<Bitmap> listener = new ProfileConnectNetwork.TaskCompleted<Bitmap>() {
            @Override
            public void onTaskCompleted(Bitmap response) {
                final Bitmap bitmap = response;
                pView.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);

                       pView.hideProgress();

                        if(!db.selectPhoto(db,new UserAttributes().getInstance().getMapArguments().get("id"))){
                            try{
                            ByteArrayOutputStream buffer = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight());
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, buffer);
                            db.addEntry(new UserAttributes().getInstance().getMapArguments().get("id"),  buffer.toByteArray() );}
                            catch (Exception e){
                                e.printStackTrace();
                            }

                        }


                    }
                });
            }
        };



        ProfileConnectNetwork<Bitmap> con = new ProfileConnectNetwork<Bitmap>(listener, Bitmap.class);
        try {
            con.execute().get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            con.cancel(true);
            Bitmap  bitmap = db.onDataLoadFromDB(db, new UserAttributes().getInstance().getMapArguments().get("id"));
            imageView.setImageBitmap(bitmap);
            hideProgressBar();

            Log.e("MyError", "InterruptedException -- ListPresenter");
        } catch (ExecutionException e) {
            e.printStackTrace();
            con.cancel(true);
            Bitmap  bitmap = db.onDataLoadFromDB(db, new UserAttributes().getInstance().getMapArguments().get("id"));
            imageView.setImageBitmap(bitmap);
            hideProgressBar();

            Log.e("MyError", "ExecutionException -- ListPresenter");
        } catch (TimeoutException e) {
            e.printStackTrace();
            con.cancel(true);
            Bitmap  bitmap = db.onDataLoadFromDB(db, new UserAttributes().getInstance().getMapArguments().get("id"));
            imageView.setImageBitmap(bitmap);
            hideProgressBar();
            Log.e("MyError", "TimeoutException -- ListPresenter");
        }
    }




}
