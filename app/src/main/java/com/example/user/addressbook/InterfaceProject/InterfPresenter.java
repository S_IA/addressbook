package com.example.user.addressbook.InterfaceProject;

import com.example.user.addressbook.Setting;

public interface InterfPresenter {
    void runProgtressBar();

    void hideProgressBar();

    void getMessage(String string);

    void onDestroy();

    Setting getSetting();
}
