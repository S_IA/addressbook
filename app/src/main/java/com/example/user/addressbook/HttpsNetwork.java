package com.example.user.addressbook;

import android.graphics.BitmapFactory;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class HttpsNetwork<T> {

    private static HttpsNetwork instance;
    private OkHttpClient client;
    private static final String TAG_CALL = "tag_call";

    public HttpsNetwork() {
        client = new OkHttpClient();
    }

    public HttpsNetwork getInstance() {
        if (instance == null) {
            instance = new HttpsNetwork();
        }
        return instance;
    }

    /**
     * Получение и конвертация json пакета
     *
     * @param path  -    the {@code String} to parse as a URL.
     * @param value - тип данных для конвертации пакета
     * @return
     */
    private T getData(String path, Class<T> value) throws MalformedURLException {


        Map<String, String> arg = new UserAttributes().getInstance().getMapArguments();

        URL url = new URL("https", UrlAddress.WAY, path +
                UrlAddress.LOGIN + arg.get("login") + UrlAddress.PASSWORD + arg.get("password"));


        client.connectTimeoutMillis();

        Request request = new Request.Builder()
                .url(url)
                .tag(TAG_CALL)
                .build();

        T data = null;
        try {
            client.connectTimeoutMillis();
            Response response = client.newCall(request).execute();


            if (response.body() != null) {
                data = new Gson().fromJson(response.body().string(), (Type) value);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Запрос аутентификации
     *
     * @param value - тип данных
     * @return
     */
    public T helloy(Class<T> value) throws MalformedURLException {
        return getData(UrlAddress.HELLO, value);
    }

    /**
     * Запрос получения списка
     *
     * @param value - тип данных
     * @return
     */
    public T getAll(Class<T> value) throws MalformedURLException {
        return getData(UrlAddress.GET_ALL, value);
    }

    /**
     * Запрос получения фотографии
     *
     * @return
     */
    public T getWPhoto() throws MalformedURLException {
        T data = null;
        Map<String, String> arg = new UserAttributes().getInstance().getMapArguments();

        URL url = new URL("https", UrlAddress.WAY, UrlAddress.GET_WPHOTO +
                UrlAddress.LOGIN + arg.get("login") +
                UrlAddress.PASSWORD + arg.get("password") +
                UrlAddress.ID + arg.get("id"));


        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                try {
                    data = (T) BitmapFactory.decodeStream(response.body().byteStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    public void cancelAll() {

//        response1.close();
    }
}
