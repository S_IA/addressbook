package com.example.user.addressbook.launch;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.addressbook.InterfaceProject.InterfView;
import com.example.user.addressbook.R;
import com.example.user.addressbook.listDepartments.ListActivity;

import static android.widget.Toast.LENGTH_LONG;
import static com.example.user.addressbook.R.color.colorBlack;

public class LaunchActivity extends AppCompatActivity implements InterfView {

    private static final String LOG_TAG = "myLog";
    private EditText mLogin;
    private EditText mPassword;
    private Button mEnter;
    private Button mDeleteUser;
    private EditText lSetting;
    private ProgressBar progress;
    private CheckBox mCheck;

    private LaunchPresenter mPresentary;

    @SuppressLint("StaticFieldLeak")
    public static Context contextOfApplicate;
    //----Нажатие клавишы----//
    private View.OnClickListener mEnterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            showProgress();
            mPresentary.setUserAttridutes(
                    mLogin.getText().toString(),
                    mPassword.getText().toString());

            onButtonWasClickedEnter();


        }
    };

    //---------Регистрация------------//
    private View.OnClickListener mEnterClick2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mEnter.setText("Воити");
            mEnter.setOnClickListener(mEnterClick);

            mDeleteUser.setEnabled(true);
            mDeleteUser.setVisibility(View.VISIBLE);


        }
    };

    //--------------Сброс пользователя---------------//
    private View.OnClickListener mDeleteUserClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            boolean hasVisited = false;

            if (!hasVisited) {
                //  mPresentary.getSetting().deletedUser();
                mEnter.setText("Регистрация");
                mEnter.setOnClickListener(mEnterClick2);
                mDeleteUser.setEnabled(true);
                mDeleteUser.setVisibility(View.GONE);
            }

        }
    };
    private View.OnClickListener mCheckClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mPresentary.getSetting().setSaveUser(mLogin.getText().toString(), mPassword.getText().toString());
            mPresentary.getSetting().hasVisitedFalse(mLogin.getText().toString(), mPassword.getText().toString());

        }
    };


    //----Проверка поля текста----//
    private boolean isTextValid(TextView textView) {
        return !TextUtils.isEmpty(textView.getText());
    }

    public boolean isUserValid(TextView login, TextView password) {
        if (isTextValid(login) && isTextValid(password)) {
            return true;
        }
        return false;
    }

    //----Создание----//

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_launch);


        contextOfApplicate = getApplicationContext();

        mPresentary = new LaunchPresenter(this);

        mLogin = findViewById(R.id.laLogin);
        mPassword = findViewById(R.id.laPassword);
        mEnter = findViewById(R.id.laButtonEnter);
        mDeleteUser = findViewById(R.id.laButtonDel);
        lSetting = findViewById(R.id.la1);
        progress = findViewById(R.id.laProgress);
        mCheck = findViewById(R.id.saveUser);

        mEnter.setOnClickListener(mEnterClick);
        mDeleteUser.setOnClickListener(mDeleteUserClick);


        mCheck.setChecked(mPresentary.getSetting().getSaveUser());


        if (mPresentary.getSetting().getSaveUser()) {
            mLogin.setText(mPresentary.getSetting().getSettigLogin());
            mPassword.setText(mPresentary.getSetting().getSettingPass());
        }

        mEnter.setText("Войти");
        lSetting.setText(mPresentary.getSetting().getSettigLogin() + " " + mPresentary.getSetting().getSettingPass());

        mCheck.setOnClickListener(mCheckClick);

        if (!mPresentary.getSetting().getSettingVisited()) {

            mEnter.setText("Регистрация");
            mEnter.setOnClickListener(mEnterClick2);
            mDeleteUser.setEnabled(false);
            mDeleteUser.setVisibility(View.GONE);
            mCheck.setChecked(false);

        }
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void showText(String string) {
        Toast toast = Toast.makeText(this, string, LENGTH_LONG);
        toast.getView().setBackgroundColor(colorBlack);
        toast.show();
    }

    @Override
    public void showMessage(@StringRes int string) {
        Toast.makeText(this, string, LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(ProgressBar.GONE);
    }

    @Override
    public Context getContext() {
        return contextOfApplicate;
    }


    @Override
    public void onButtonWasClickedEnter() {


        if (isUserValid(mLogin, mPassword)) {
            mPresentary.beginAuthem();
        } else {
            showMessage(R.string.null_user);
        }


    }

    @Override
    public void newActivity() {
        Intent intent = new Intent(LaunchActivity.this, ListActivity.class);
        startActivity(intent);
        LaunchActivity.this.finish();

    }


    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
        mPresentary.onDestroy();
        mPresentary = null;
    }

    @Override
    public void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Связь с сервером не установлена");
        alertDialog.setMessage("Повторить запрос?");
        alertDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showProgress();
                onButtonWasClickedEnter();
            }
        });
        alertDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.show();
    }

    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(LOG_TAG, "onRestoreInstanceState");
    }

    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume ");
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(LOG_TAG, "onSaveInstanceState");
    }

    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    public void onclick(View v) {
    }
}

