package com.example.user.addressbook.listDepartments;

import android.os.AsyncTask;

import com.example.user.addressbook.HttpsNetwork;

import java.net.MalformedURLException;

public class ListConnectNetwork<T> extends AsyncTask<Void, Void, Void> {


    public interface TaskCompleted<T> {
        void onTaskCompleted(T response);
    }

    private TaskCompleted<T> listener;
    private Class<T> value;


    public ListConnectNetwork(TaskCompleted listener, Class<T> clazzT) {
        this.listener = listener;
        this.value = clazzT;;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

    @Override
    protected Void doInBackground(Void... voids) {


        T data = null;
        try {
            data = (T) new HttpsNetwork<T>().getInstance().getAll(value);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if( !isCancelled()){
        listener.onTaskCompleted(data);
        }
        return null;

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        new  HttpsNetwork<T>().getInstance().cancelAll();
    }
}
