package com.example.user.addressbook.profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.user.addressbook.ControlDbHelper;
import com.example.user.addressbook.InterfaceProject.InterfView;
import com.example.user.addressbook.R;
import com.example.user.addressbook.listDepartments.Employees;

import static android.widget.Toast.LENGTH_LONG;
import static com.example.user.addressbook.R.color.colorBlack;


public class ProfileActivity extends AppCompatActivity implements InterfView {


    private ImageView mImage;
    private TextView mName;
    private TextView mTitle;
    private TextView mMail;
    private TextView mPhone;
    private Toolbar mTools;

    private ProgressBar progress;
    private ProfilePresenter pPresenter;
    private ControlDbHelper mDbHelper;


       @SuppressLint("ResourceAsColor")
    @Override
    public void showText(String string) {
        Toast toast = Toast.makeText(this, string, LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.getView().setBackgroundColor(colorBlack);
        toast.show();
    }

    @Override
    public void showMessage(int string) {
        Toast.makeText(this, string, LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(ProgressBar.GONE);
    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void onButtonWasClickedEnter() {

    }

    @Override
    public void newActivity() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void showAlertDialog() {

    }


    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_profile);

        setTitle("Профиль");

        progress = findViewById(R.id.progress);
        mImage = findViewById(R.id.acImage);
        mName = findViewById(R.id.acName);
        mTitle = findViewById(R.id.acTitle);
        mMail = findViewById(R.id.acMail);
        mPhone = findViewById(R.id.acPhone);

        mDbHelper = new ControlDbHelper(this);

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Employees employe = (Employees) getIntent().getParcelableExtra("Employe");

        if (employe.getName() != null)
            mName.setText(employe.getName());

        if (employe.getTitle() != null)
            mTitle.setText(employe.getTitle());

        if (employe.getEmail() != null) {
            mMail.setText(employe.getEmail());
            Linkify.addLinks(mMail, Linkify.EMAIL_ADDRESSES);
        }
        if (employe.getPhone() != null){
            mPhone.setText(employe.getPhone());
            Linkify.addLinks(mPhone, Linkify.PHONE_NUMBERS);
        }


        pPresenter = new ProfilePresenter(this, mImage);
        pPresenter.getImage(mDbHelper);

        db.close();

    }






}
