package com.example.user.addressbook.listDepartments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.addressbook.ControlDbHelper;
import com.example.user.addressbook.ExampleApplication;
import com.example.user.addressbook.InterfaceProject.InterfView;
import com.example.user.addressbook.R;
import com.example.user.addressbook.UserAttributes;
import com.example.user.addressbook.profile.ProfileActivity;
import com.squareup.leakcanary.RefWatcher;

import static android.widget.Toast.LENGTH_LONG;
import static com.example.user.addressbook.R.color.colorBlack;

public class ListActivity extends AppCompatActivity implements InterfView {

    DepartsmentAdapter adapter;
    private ProgressBar progress;
    private ListPresenter listPresenter;
    public static Context contextOfApplicate;
    ListView mList;
    private ControlDbHelper mDbHelper;


    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            setTitle("Список сотрудников");



        mList = findViewById(R.id.listView);
        progress = findViewById(R.id.liProgress);

        listPresenter = new ListPresenter(this);

        contextOfApplicate = getApplicationContext();
        mDbHelper = new ControlDbHelper(this);


        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        listPresenter.getList(mDbHelper, mList);


        db.close();
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                adapter = listPresenter.getAdapter();
                boolean a = adapter.clickOnItem(position);

                if (a) {


                    Intent intent = new Intent(ListActivity.this, ProfileActivity.class);

                    Employees employe = adapter.getItemEmployees(position);

                    new UserAttributes().getInstance().setID(employe.getId());
                    intent.putExtra("Employe", employe);

                    startActivity(intent);

                }

            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();
        listPresenter.onStart(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        listPresenter.onStop();
    }

    // onResume (onStart) - подписываешь вьюху на презентер
    // onPause (onStop) - отписываешь

    @SuppressLint("ResourceAsColor")
    @Override
    public void showText(String string) {
        Toast toast = Toast.makeText(this, string, LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.getView().setBackgroundColor(colorBlack);
        toast.show();
    }

    @Override
    public void showMessage(int string) {
        Toast.makeText(this, string, LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(ProgressBar.GONE);
    }

    @Override
    public Context getContext() {
        return contextOfApplicate;
    }


    @Override
    public void onButtonWasClickedEnter() {

    }

    @Override
    public void newActivity() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ExampleApplication.getRefWatcher(this);
        refWatcher.watch(this);
        refWatcher.watch(listPresenter);
    }

    @Override
    public void showAlertDialog() {

    }


}
