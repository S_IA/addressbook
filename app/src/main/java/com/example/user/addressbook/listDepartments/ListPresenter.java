package com.example.user.addressbook.listDepartments;

import android.util.Log;
import android.widget.ListView;

import com.example.user.addressbook.ControlDbHelper;
import com.example.user.addressbook.InterfaceProject.InterfPresenter;
import com.example.user.addressbook.Setting;
import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ListPresenter implements InterfPresenter {

    private ListActivity lView;
    ;
    private DepartsmentAdapter adapter;


    public ListPresenter(ListActivity lView) {
        this.lView = lView;
    }

    @Override
    public void runProgtressBar() {
        lView.showProgress();
    }

    @Override
    public void hideProgressBar() {
        lView.hideProgress();
    }

    @Override
    public void getMessage(String string) {
        lView.showText(string);
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public Setting getSetting() {
        return null;
    }


    public void getList(final ControlDbHelper dbHelper, final ListView mList) {

        ListConnectNetwork.TaskCompleted<Department> listener = new ListConnectNetwork.TaskCompleted<Department>() {
            @Override
            public void onTaskCompleted(final Department response) {

                lView.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String valu = new Gson().toJson(response);
                        dbHelper.updateRecord(1, valu);
                        adapter = new DepartsmentAdapter(lView.getContext(), response);
                        mList.setAdapter(adapter);
                        hideProgressBar();
                    }
                });
            }

        };


        ListConnectNetwork<Department> con = new ListConnectNetwork<>(listener, Department.class);
        try {
            con.execute().get(5, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            Department department = dbHelper.extractDepartment(dbHelper);
            adapter = new DepartsmentAdapter(lView.getContext(), department);
            con.cancel(true);
            mList.setAdapter(adapter);
            hideProgressBar();
            Log.e("MyError", "TimeoutException -- ListPresenter");
        } catch (InterruptedException e) {
            e.printStackTrace();
            Department department = dbHelper.extractDepartment(dbHelper);
            adapter = new DepartsmentAdapter(lView.getContext(), department);
            con.cancel(true);
            hideProgressBar();
            Log.e("MyError", "InterruptedException -- ListPresenter");
        } catch (ExecutionException e) {
            e.printStackTrace();
            Department department = dbHelper.extractDepartment(dbHelper);
            adapter = new DepartsmentAdapter(lView.getContext(), department);
            con.cancel(true);
            hideProgressBar();
            Log.e("MyError", "ExecutionException -- ListPresenter");
        }

    }


    public DepartsmentAdapter getAdapter() {
        return adapter;
    }

    public void onStart(ListActivity view) {
        this.lView = view;
    }

    public void onStop() {
        this.lView = null;
    }
}
