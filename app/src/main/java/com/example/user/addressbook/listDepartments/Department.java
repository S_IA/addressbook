package com.example.user.addressbook.listDepartments;


import com.example.user.addressbook.R;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Department {

    @SerializedName("ID")
    private String id;
    @SerializedName("Name")
    private String name;
    @SerializedName("Departments")
    private ArrayList<Department> departments;
    @SerializedName("Employees")
    private ArrayList<Employees> employeeses;

    /**
     * Получение id
     */
    public String getId() {
        return id;
    }

    /**
     * Получение имени
     */
    public String getName() {
        return name;
    }

    /**
     * Получение списка departments
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Получение списка eployesses
     */
    public ArrayList<Employees> getEmployees() {
        return employeeses;
    }

    /**
     * Задаем id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Задаем имя
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Задаем элемент списка Department
     */
    public void setItemDepartment(Department department) {
        this.departments.add(department);
    }

    /**
     * Задаем элемен списка Eployess
     */
    public void setItemEployess(Employees eployess) {
        this.employeeses.add(eployess);
    }

    /**
     * Инициируем список Employees
     */
    public void initiateEmployees() {
        this.employeeses = new ArrayList<Employees>();
    }

    /**
     * Инициируем список Departments
     */
    public void initiateDepartments() {
        this.departments = new ArrayList<Department>();
    }

    /**
     * Получение визуального обозначения
     */
    public int getIconResource() { // Метод возвращающий картинку. Если есть потомки, то будет иконка в виде папки, если нет, то в виде файла.

        return R.drawable.baseline_folder_shared_black_18dp;

    }


}
